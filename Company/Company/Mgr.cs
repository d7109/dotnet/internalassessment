﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Company
{
    internal class Mgr : Emp
    {
        private double _performanceBonus;

        public double performanceBonus
        {
            get { return _performanceBonus; }
            set { _performanceBonus = value; }
        }

        public void acceptmgr()
        {
            this.accept();
            Console.WriteLine("Enter performance bonus");
            this._performanceBonus = Convert.ToDouble(Console.ReadLine());
        }

        public double netSalary()
        {
            return (this.basic+this.performanceBonus);
        }

        public override string ToString()

        {
            double netsalary = netSalary();
            return ("Id: " + this.id + ", " + "Name: " + this.name + ", " + "DeptId: " + this.deptId + ", " + "Basic: " + this.basic + ", " + "Performance Bonus: " +this.performanceBonus + ", " + "Net salary: " + netsalary);
        }

    }
}
